# Challenge_1_MDEF_Sheet_Masks

Week from February 15 to 19 2021. Challenge n ° 1 of the MDEF Master. I worked with Bothaina around cosmetics. We chose to work on sheet masks for this challenge.

<html>

<!-- !PAGE CONTENT! -->
<div class="w3-content" style="max-width:1500px">

  <!-- Header -->
  <div class="w3-row">
  <div class="w3-half w3-container">
    <h1 class="w3-xxlarge w3-text-light-grey">February 2021</h1>
    <h1 class="w3-xxlarge w3-text-grey">Week 15/19 </h1>
    <h1 class="w3-jumbo">Challenge 1</h1>
    <h1 class="w3-xxlarge w3-text-light-grey">Sheet masks</h1>
    <h1 class="w3-xxlarge w3-text-grey">Do you really know what's in your sheet masks ? </h1>
    <h1 class="w3-jumbo">Sheet masks</h1>
  </div>
  <div class="w3-half w3-container w3-xxlarge w3-text-grey">
    <p class="">The Surprisingly Scary Ingredient Hiding in Your Sheet Mask article (1) written by LINDSEY METRUS</p>
  </div>
  </div>

  <!-- Photo Grid -->

![](Images/article_picture_1.jpg)

Sheet masks are so much more than a skin care product - they're calming, they're Instagram fodder, they're a nighttime activity for girls ... usually for a few bucks they're the kind of guy. photogenic indulgence we crave in a stressful, social media-heavy climate.

Over the past few years, sheet mask culture has exploded and is projected to steadily increase: According to statistics website Statista, in 2018, the global sheet mask market was estimated to be valued at about 282.8 million USD and is forecasted to reach 551.3 million dollars by 2026. South Korea, the genesis of sheet masks, is still the top producer (41% of the market), with China next in line at 22% and the U.S. only responsible for 2%. Comparatively, we may not be producing as many masks on U.S. soil, but stores and brands are certainly buying en masse from Asian companies—it seems like everywhere you turn, a sheet mask is available for purchase.

Your ROI is impressive too, and afterward, your skin is noticeably more dewy, fresh, and glowing than you were pre-mask. What you may be surprised to know, though, is that the smooth, supple complexion you get after sheet-masking comes at much more of a cost than a few George Washingtons.

A few years back, a south Chinese publication reported on the use of steroids in sheet masks, specifically glucocorticoid, a topical used to treat skin inflammation and allergies. Through continued use, the skin may become dependent on the steroid, which could lead to allergies, inflammation, pimples, and expansion of capillaries once you stop using it. And considering how often we use them (some people tout using a sheet mask every day), it's easy for that dependency to build up.

So why are brands using steroids in their sheet masks in the first place? Turns out they offer immediate positive results. "Steroids act fast and give visible results on the skin immediately, tricking customers into thinking that these are high-performance products," explains Phoebe Song, founder of Snow Fox skincare. "The brands are not named due to the ongoing investigation by authorities, but according to the Chinese FDA, 1 in 4 sheet masks contain banned steroids (glucocorticoid). The most frightening part of all this is that it only takes two weeks for users to develop a dependency."

Despite being a banned ingredient, it's confusing, then, that glucocorticoid is still sneaking its way into sheet masks. But because of the mass amount of masks that are produced on a daily basis, authorities simply can't keep up with production.

"Because sheet masking is a staple of Asian skincare, there is a ton of increasing demand each year, with producers scrambling to make as much volume as possible," says Song. "Banned ingredients are definitely not allowed and Asian authorities are trying their best to regulate cosmetics and beauty products, but it is very difficult to test every single item each day due to the sheer volume of manufacturers. Regulatory bodies are inundated, and sometimes, brands go as far as to mislabel ingredients—even big corporate brands.

I think this is a global problem in the beauty industry–Asia is simply much bigger in terms of size, so it's even harder to catch bad products before they enter the marketplace.”

Using harmful ingredients in skincare isn't a shocking revelation—it's happening every day here in the States. And according to Song, the U.S. is guilty of putting steroids in its products as well. "I've definitely seen them in the U.S., but not so much in sheet masks, more so in serums and creams," she says. "I've also seen hydro-cortisol (another steroid) which surprised me as I thought it would be stricter in the U.S.

So what else should you look out for in your sheet masks? “Steroids aside (avoid anything that sounds like corticoid, cortisol, etc), I've seen ones with lots of perfuming agents such as citral or harsh preservatives like denatured alcohol etc.," says Song. "I've also seen masks with very little active ingredients (usually the cheap ones), where it's mainly just glycerin, fragrance and water. If a mask retails at $2-4 USD, you can imagine the costs they need to make a margin.” Plus, there was that whole exposé written by Racked where the site revealed that sheet masks are being packaged using at-home labor in Korea in unsanitary conditions by workers who aren't wearing gloves, meaning you're getting bacteria and other debris in your skincare.

Moral of the story: Cheaper isn't always better, especially when it comes to caring for the largest organ in the body (your skin), and researching the company that makes your product as well as vetting the ingredient list is imperative, whether it's a sheet mask or any skincare product.

</p>

![](Images/multi-facing.jpg)

<p class="w3-xlarge">

We wanted to work around cosmetics to continue our intervention. We already engaged with Bothaina in the last intervention we did on the beach with people and ask them if they were open to the idea to reused materials from home and make their own scrubs, soaps, masks, makeup. We had a good feedback from them that they might be interesting. This is why on this challenge we focus our project on the face and more especially sheet masks with the reference of the 3D printing machine. (3)
</p>

<h1 class="w3-jumbo"> The goal </h1>

<p class="w3-xlarge">

The goal is to make them try the mold we have been designing to cooked our own gel facial mask and to know what’s inside of it!
During the quarantaine or even during this stressing period a lot of people turned into more confort at home or self-care. (4)

After some research we find this article « The Surprisingly Scary Ingredient Hiding in Your Sheet Mask article (1) written by LINDSEY METRUS » make us realize we don’t really what’s inside the masks we are using every week mostly.
Also one specific masks does not necessarily fit your need. The idea is based on the multi-masking technic to fit better any type of skins. Picture 2

Generally the T zone is more oily and the cheeks are more dry as the front but it depends. We checked some references and try to find the materials (natural) used in those masks depending on what you wanted to get.

Some examples for future recipes

Research : Rose water, honey and glycerin are very often noted.

Detox Mask with argile and green tea.To eliminate the impurity.
Relax Mask and make it more smooth, with salt sea and algae.
Rescue Mask with butter (karité?) and grapes leafs great to hydrate and make it soft.
Glow Mask with beetroot, rose water, lemon, honey.
Anti-oxydant against extern factors with algae, Goji berries and quinoa seeds.

Or Algae like saltwort to hydrate / Agar agar cold ? And of course Aloe vera.
</p>

<h1 class="w3-jumbo">Visual</h1>


  ![](Images/photos_fullface.jpg)

<h1 class="w3-jumbo">Process</h1>


<p class="w3-xlarge">
We started by doing buying masks from the market to reuse the same size for the mold of the face. And we started to draw some different zones inside the face mold. The idea is to proposed a mold that is personalized and gender included. You can fill in only some zone of the mold, for example just the eyes or the top part if you have a beard.

  </p>

  ![](Images/mask_schema.jpg)
  ![](Images/maskdrawing.jpg)
  ![](Images/facedrawing.JPG)

  <p class="w3-xlarge">

We started to draw the mold in 3D for the laser cutter. The first step was to take the measures of the drawing and make it scale 1. After downloaded the file to rhino to cut it in the laser we had problems we had to rescale the sketch. The thursday afternoon we started to cut it. We chose acrylic of 5 mm for the mold and for the top part we chose 5 mm to make it homogeneous and heavy for the structure. After 2 passages with the laser we finally cut it trough.

</p>

![](Images/drawing_software.jpg)
![](Images/cutting_laser.jpg)
![](Images/cuttingtest_1.jpg)
![](Images/prototype_with_drawing.jpg)

<p class="w3-xlarge">

On Friday we had to cut the mold again because something went wrong, we don't have the right proportion. The bottom circle was too short for the border that comes at the top. So we decided to reprint it, and check that the circle was 23 cm in the same way as the border. We also had to redesign the front part (we took the outer line instead of the inner) and the parts under the eyes. Each mold inside the lower face circle will have a lower part to prevent the liquid gel face mask recipe from coming out.
We will print the top part to close the mold like a box also using the 5mm acrylic (the size of the top part is 23.6cm to be a little bigger than the mold which is 23cm for the adapt).

Here you can find the link to the rhino file to re print the face mold.

</p>

[This is the link to the rhino file.](Files/sheet mask cutting fail.3dm)

<h1 class="w3-jumbo">Recipes</h1>

<p class="w3-xlarge">

We also did some experiment during this challenge of recipes that might be used to fill in the face mold by using biomaterials recipes.

Recipe 1

We used collagen

5 gr of collagen
25 ml of juice
70 ml of water

1. We boiled the mixed and put it in a mold and let it cold in the fridge all night. It didn't work the mixture is still very liquide we can't used this recipe for the gel facial masks.

</p>

![](Images/collagen_recipe.jpg)
![](Images/collagen_juice.jpg)

<p class="w3-xlarge">

Recipe 2

We used alginate bioplastic recipe from Fab Lab

2 gr alginate
2 gr of orange peels
2 gr oil
33 gr of water
5 gr glycerin

You have to used CaCl2 (7 gr for 100 ml of water)
And spray it before and after you put the alginate on the sketch drawing of the face.

</p>

![](Images/alginate_recipe.jpg)
![](Images/alginate_test_face.jpg)

<h1 class="w3-jumbo">Next challenges</h1>

<p class="w3-xlarge">

1. The next steps of this project is to give the face mold (fullface project) to 2 people with 2 recipes and make them try and gather their feedbacks.
2. Depending on the feedbacks we will change the design of the face mold and add some modifications.
3. We want to create a platform to exchange recipes for sheet masks and other cosmetics/ non cosmetics products.
4. We could measures or research the quantity of materials used for each sheet masks and how much they saved from doing cosmetics at home.
5. Analyse the results on their skin.
6. Analyse the materials waste saved or consumed.
7. To try scan the faces of different people to make a different/ personalized mold for each and see the difference.
8. Use the fcae mold for the microbiome education of our own skin. (4)
9. In this project we want to make people more aware of the products that they are using and possibilities that they have to make their own cosmetics and personalized treatments. It's also very important for us to make the project (fullface) gender included.

</p>

<p class="w3-xlarge">
Bibliography :</p>
<p class="w3-large ">
<a href="https://www.byrdie.com/steroids-in-sheet-masks">(1) The Surprisingly Scary Ingredient Hiding in Your Sheet Mask, article written by Lindsey Metrus.</a></p>
<p class="w3-large ">
<a href="https://www.grandviewresearch.com/industry-analysis/sheet-face-mask-market">(2) Sheet Face Mask Market Size, Share & Trends Analysis Report By Product Type (Cotton, Non-woven, Hydrogel, Bio-cellulose), By Region, And Segment Forecasts, 2019 - 2025, Published Date: Jul, 2019, Report ID: GVR-3-68038-226-6.</a></p>
<a href="https://www.wish.com/product/5f9a87bc14b7771c00d3188d?hide_login_modal=true&from_ad=goog_shopping&_display_country_code=ES&_force_currency_code=EUR&pid=googleadwords_int&c=%7BcampaignId%7D&ad_cid=5f9a87bc14b7771c00d3188d&ad_cc=ES&ad_lang=EN&ad_curr=EUR&ad_price=24.00&campaign_id=9044277795&exclude_install=true&gclid=Cj0KCQiA4L2BBhCvARIsAO0SBdblfl5EO3HXbU1nEQmEkW6d_5MbYTOVjAGECz3z_u-B--9jcsMJqQ4aAn93EALw_wcB&share=web
">(3) Skin Care Machine to printing your sheet masks at home.</a></p>
<p class="w3-large ">
<a href="https://hudabeauty.com/us/en_US/blog/how-to-make-sheet-masks-34854.html
"> Huda beauty.</a></p>
<a href="https://www.akane-skincare.com/42-masques"> Akane skincare.</a></p>
<p class="w3-large ">




<!-- End Page Content -->
</div>


</body>
</html>
